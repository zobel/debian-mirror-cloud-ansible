# Debian Mirror Cloud Ansible

## SSH config

```
Host *.debian.org *.debian.net *.debian.invalid
  User $DEBIANUSER

Host *.debian-mirror-v2-prod.azure.debian.invalid
  ProxyJump jump.azure-mirror.debian.net

Host *.debian-mirror-v2-staging.azure.debian.invalid
  ProxyJump jump.azure-mirror-staging.debian.net
```
